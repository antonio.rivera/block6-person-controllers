package com.example.block6personcontrollers.controller;

import com.example.block6personcontrollers.model.Persona;
import com.example.block6personcontrollers.service.PersonaService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class Controlador2 {

    //private final PersonaService personaService;
    private final Controlador1 controlador1;


    @GetMapping("controlador2/getPersona")
    public ResponseEntity<Persona> getPersona(){
        // Esto así no es
        Persona persona = controlador1.addUser("pepe","madrid",12).getBody();
        persona.setEdad(persona.getEdad()*2);
        return ResponseEntity.ok(persona);
    }
}
