package com.example.block6personcontrollers.controller;

import com.example.block6personcontrollers.model.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ControladorBean {
    @Autowired
    @Qualifier("bean1")
    private Persona persona1;
    @Autowired
    @Qualifier("bean2")
    private Persona persona2;
    @Autowired
    @Qualifier("bean3")
    private Persona persona3;
    @GetMapping("/controlador/bean/{bean}")
public ResponseEntity<Persona> getBean(@PathVariable String bean){

        if("bean1".equals(bean)){
            return ResponseEntity.ok(persona1);
        }else if("bean2".equals(bean)){
            return ResponseEntity.ok(persona2);
        }else{
            return ResponseEntity.ok(persona3);
        }

    }
}
