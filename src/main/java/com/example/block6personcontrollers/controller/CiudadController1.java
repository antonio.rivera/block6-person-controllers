package com.example.block6personcontrollers.controller;

import com.example.block6personcontrollers.model.Ciudad;
import com.example.block6personcontrollers.service.CiudadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class CiudadController1 {
    @Autowired
    private CiudadService ciudadService;

    @PostMapping("/controlador1Ciudad/addCiudad")
    public ResponseEntity<Ciudad> addCiudad(@RequestBody Ciudad ciudad){
        Ciudad ciudad1 = ciudadService.addCiudad(ciudad);
        return ResponseEntity.ok(ciudad1);
    }

}
