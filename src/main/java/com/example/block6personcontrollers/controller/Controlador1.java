package com.example.block6personcontrollers.controller;


import com.example.block6personcontrollers.model.Persona;
import com.example.block6personcontrollers.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;


@Controller
public class Controlador1 {
    @Autowired
    private PersonaService personaService;

    @GetMapping("/controlador1/addPersona")
    public ResponseEntity<Persona> addUser(@RequestHeader String nombre
                                            ,@RequestHeader String poblacion
                                            ,@RequestHeader int edad){

        Persona persona = personaService.addUser(nombre,poblacion,edad);

        return ResponseEntity.ok(persona);

    }

}
