package com.example.block6personcontrollers.controller;


import com.example.block6personcontrollers.model.Ciudad;
import com.example.block6personcontrollers.service.CiudadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class CiudadController2 {

    @Autowired
    private CiudadService ciudadService;

    @GetMapping("/controlador2Ciudad/getCiudades")
    public ResponseEntity<List<Ciudad>> getCiudades(){
        List<Ciudad> ciudades = ciudadService.getCiudades();
        return ResponseEntity.ok(ciudades);
    }
}
