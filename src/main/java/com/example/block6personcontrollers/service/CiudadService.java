package com.example.block6personcontrollers.service;

import com.example.block6personcontrollers.model.Ciudad;

import java.util.List;

public interface CiudadService {
    Ciudad addCiudad(Ciudad ciudad);

    List<Ciudad> getCiudades();
}
