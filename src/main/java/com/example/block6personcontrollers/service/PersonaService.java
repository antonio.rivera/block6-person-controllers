package com.example.block6personcontrollers.service;

import com.example.block6personcontrollers.model.Persona;

public interface PersonaService {
    Persona addUser(String nombre, String poblacion, int edad);
}
