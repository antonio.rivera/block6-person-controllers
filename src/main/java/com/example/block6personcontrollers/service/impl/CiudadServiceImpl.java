package com.example.block6personcontrollers.service.impl;

import com.example.block6personcontrollers.model.Ciudad;
import com.example.block6personcontrollers.repository.CiudadRepository;
import com.example.block6personcontrollers.service.CiudadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CiudadServiceImpl implements CiudadService {

    @Autowired
    private CiudadRepository ciudadRepository;
    @Override
    public Ciudad addCiudad(Ciudad ciudad) {
        return ciudadRepository.save(ciudad);
    }

    @Override
    public List<Ciudad> getCiudades() {
        return ciudadRepository.findAll();
    }
}
