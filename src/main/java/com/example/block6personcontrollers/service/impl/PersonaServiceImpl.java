package com.example.block6personcontrollers.service.impl;

import com.example.block6personcontrollers.model.Persona;
import com.example.block6personcontrollers.repository.PersonaRepository;
import com.example.block6personcontrollers.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonaServiceImpl implements PersonaService {

    @Autowired
    private PersonaRepository personaRepository;

    @Override
    public Persona addUser(String nombre, String poblacion, int edad) {
        Persona persona = new Persona(nombre,poblacion,edad);
        return personaRepository.save(persona);
    }
}
