package com.example.block6personcontrollers.config;

import com.example.block6personcontrollers.model.Persona;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigurationPersona {

    @Bean
    @Qualifier("bean1")
    public Persona persona1(){
        return new Persona(50L,"Juanjose","Ubeda",19);
    }
    @Bean
    @Qualifier("bean2")
    public Persona persona2(){
        return new Persona(51L,"Javier","Jodar",20);
    }
    @Bean
    @Qualifier("bean3")
    public Persona persona3(){
        return new Persona(52L,"Francisco","Jodar",25);
    }
}
